package com.rxh5.codefest_round_1;


import com.rxh5.codefest_round_1.model.Item;
import com.rxh5.codefest_round_1.model.ItemCollectionRequest;
import com.rxh5.codefest_round_1.model.Shift;
import com.rxh5.codefest_round_1.model.ShiftRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CodefestFinalApplicationTests {

    private WebTestClient webTestClient = WebTestClient.bindToServer().baseUrl("http://localhost:8080").build();

    @Test
    public void testIOTDevice1() {
        Random random = new Random();

        Item[] items = new Item[50];

        for (int i = 0; i < 50; i++) {
            items[i] = new Item(random.nextBoolean());
        }

        ItemCollectionRequest request = new ItemCollectionRequest(50, items, 1, LocalDateTime.now());

        webTestClient.post().uri("/items")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .body(Mono.just(request), ItemCollectionRequest.class)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectBody()
                .jsonPath("$.id").isNotEmpty();

    }

    @Test
    public void testReportsHours() {
        webTestClient.get().uri("/report?startAt=10/3/2018&endAt=10/4/2018&period=hour&stage=1&employ=e1&manager=m1")
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectBody();
    }

    @Test
    public void testReportsDays() {
        webTestClient.get().uri("/report?startAt=10/3/2018&endAt=10/4/2018&period=day&stage=1&employ=e1&manager=m1")
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectBody();
    }

    @Test
    public void testReportsMonths() {
        webTestClient.get().uri("/report?startAt=10/3/2018&endAt=10/4/2018&period=month&stage=1&employ=e1&manager=m1")
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectBody();
    }

    @Test
    public void testEmployeeReports() {
        webTestClient.get().uri("/employeeReport?endAt=10/5/2018")
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectBody();
    }

    @Test
    public void testNotifications() {
        webTestClient.get().uri("/notification")
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectBody()
                .jsonPath("$.notifications").exists();
    }


    @Test
    public void testShiftMangementIntergration() {
        ArrayList<Shift> shifts = new ArrayList<>();
        shifts.add(new Shift("m1", "e1", LocalDateTime.of(2018, 10, 3, 0, 0),
                LocalDateTime.of(2018, 10, 5, 0, 0), 1));
        shifts.add(new Shift("m1", "e2", LocalDateTime.of(2018, 10, 3, 0, 0),
                LocalDateTime.of(2018, 10, 5, 0, 0), 2));
        shifts.add(new Shift("m1", "e3", LocalDateTime.of(2018, 10, 3, 0, 0),
                LocalDateTime.of(2018, 10, 5, 0, 0), 3));
        ShiftRequest request = new ShiftRequest(shifts);

        webTestClient.post().uri("/shifts")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .body(Mono.just(request), ShiftRequest.class)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectBody();
    }

    @Test
    public void testEmployeeUpdate() {
        ArrayList<String> employees = new ArrayList<>();
        employees.add("e1");
        employees.add("e2");
        employees.add("e3");
        webTestClient.post().uri("/updateEmployees")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .body(Mono.just(employees), List.class)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectBody();
    }

    @Test
    public void testEmployeeAnalytic() {
        webTestClient.get().uri("/processEmployAnalytics")
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectBody();
    }

}