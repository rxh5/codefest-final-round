package com.rxh5.codefest_round_1.util;

import javafx.util.Pair;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

public class PeriodUtil {

    public static List<Pair<LocalDateTime, LocalDateTime>> getPeriods(LocalDateTime startAt, LocalDateTime endAt, String
            period) {
        List<Pair<LocalDateTime, LocalDateTime>> periods = new ArrayList<>();

        if (period.equalsIgnoreCase("year")) {
            long years = ChronoUnit.YEARS.between(startAt, endAt);

            if (years == 0) {
                periods.add(new Pair<>(endAt, startAt));
            } else if (years > 0) {
                LocalDateTime date = endAt;
                do {
                    date = date.minusYears(1);
                    periods.add(new Pair<>(endAt, date));
                } while (date.isAfter(startAt));
                if (startAt.isBefore(date.plusYears(1)))
                    periods.add(new Pair<>(date.plusYears(1), startAt));
            }

        } else if (period.equalsIgnoreCase("month")) {
            long months = ChronoUnit.MONTHS.between(startAt, endAt);

            if (months == 0) {
                periods.add(new Pair<>(endAt, startAt));
            } else if (months > 0) {
                LocalDateTime date = endAt;
                do {
                    date = date.minusMonths(1);
                    periods.add(new Pair<>(endAt, date));
                } while (date.isAfter(startAt));
                if (startAt.isBefore(date.plusMonths(1)))
                    periods.add(new Pair<>(date.plusMonths(1), startAt));
            }

        } else if (period.equalsIgnoreCase("week")) {
            long weeks = ChronoUnit.WEEKS.between(startAt, endAt);

            if (weeks == 0) {
                periods.add(new Pair<>(endAt, startAt));
            } else if (weeks > 0) {
                LocalDateTime date = endAt;
                do {
                    date = date.minusWeeks(1);
                    periods.add(new Pair<>(endAt, date));
                } while (date.isAfter(startAt));
                if (startAt.isBefore(date.plusWeeks(1)))
                    periods.add(new Pair<>(date.plusWeeks(1), startAt));
            }
        } else if (period.equalsIgnoreCase("day")) {
            long days = ChronoUnit.DAYS.between(startAt, endAt);

            if (days == 0) {
                periods.add(new Pair<>(endAt, startAt));
            } else if (days > 0) {
                LocalDateTime date = endAt;
                do {
                    date = date.minusDays(1);
                    periods.add(new Pair<>(endAt, date));
                } while (date.isAfter(startAt));
                if (startAt.isBefore(date.plusDays(1)))
                    periods.add(new Pair<>(date.plusDays(1), startAt));
            }
        } else if (period.equalsIgnoreCase("hour")) {
            long hours = ChronoUnit.HOURS.between(startAt, endAt);

            if (hours == 0) {
                periods.add(new Pair<>(endAt, startAt));
            } else if (hours > 0) {
                LocalDateTime date = endAt;
                do {
                    date = date.minusHours(1);
                    periods.add(new Pair<>(endAt, date));
                } while (date.isAfter(startAt));
                if (startAt.isBefore(date.plusHours(1)))
                    periods.add(new Pair<>(date.plusHours(1), startAt));
            }
        }
        return periods;
    }

}
