package com.rxh5.codefest_round_1.controller;

import com.rxh5.codefest_round_1.model.Item;
import com.rxh5.codefest_round_1.model.ItemCollection;
import com.rxh5.codefest_round_1.model.ItemCollectionRequest;
import com.rxh5.codefest_round_1.repository.ItemRepository;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.CoreSubscriber;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.util.List;

@RestController
public class ItemController {

    private final ItemRepository itemRepository;

    @Autowired
    public ItemController(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }


    @CrossOrigin
    @GetMapping(value = "/items/{stage}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<ItemCollection> getItemsOfStage(@PathVariable(value = "stage") int stage) {
        System.out.println("getItemsOfStage " + stage);
        return itemRepository.findWithTailableCursorByStage(stage);
    }

    @PostMapping("/items")
    public Mono<ItemCollection> createItem(@Valid @RequestBody ItemCollectionRequest request) {
        return Flux.just(request.getItems())
                .filter(Item::isDefective)
                .collectList()
                .map(List::size)
                .flatMap(noOfDefected -> {
                    ItemCollection itemCollection = new ItemCollection(request.getNoOfItems(), noOfDefected, request.getStage(), request.getTimestamp());
                    return itemRepository.save(itemCollection);
                });
    }

}
