package com.rxh5.codefest_round_1.controller;

import com.rxh5.codefest_round_1.model.*;
import com.rxh5.codefest_round_1.repository.*;
import com.rxh5.codefest_round_1.util.PeriodUtil;
import javafx.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
public class ReportController {

    private final ItemRepository itemRepository;
    private final ShiftRepository shiftRepository;
    private final AnalyticRepository analyticRepository;
    private final EmployAnalyticRepository employAnalyticRepository;
    private final EmployeeRepository employeeRepository;

    private LocalDateTime lastNotificationUpdate = LocalDateTime.now();
    private LocalDateTime lastEmployAnalyticUpdate = LocalDateTime.now();

    @Autowired
    ReportController(ItemRepository itemRepository, ShiftRepository shiftRepository, AnalyticRepository analyticRepository,
                     EmployAnalyticRepository employAnalyticRepository, EmployeeRepository employeeRepository) {
        this.itemRepository = itemRepository;
        this.shiftRepository = shiftRepository;
        this.analyticRepository = analyticRepository;
        this.employAnalyticRepository = employAnalyticRepository;
        this.employeeRepository = employeeRepository;
    }

    @CrossOrigin
    @GetMapping(value = "/report")
    Mono<List<ReportElement>> getReport(@RequestParam(value = "startAt") String startAtReq,
                                        @RequestParam(value = "endAt") String endAtReq,
                                        @RequestParam(value = "period") String period,
                                        @RequestParam(value = "stage", required = false) int stage,
                                        @RequestParam(value = "employ", required = false) String employ,
                                        @RequestParam(value = "manager", required = false) String manager) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/d/yyyy");
        LocalDateTime startAt = LocalDate.parse(startAtReq, formatter).atStartOfDay();
        LocalDateTime endAt = LocalDate.parse(endAtReq, formatter).atStartOfDay();
//        LocalDateTime endAt = LocalDateTime.parse(endAtReq, formatter);

        List<Pair<LocalDateTime, LocalDateTime>> periods = PeriodUtil.getPeriods(startAt, endAt, period);

        Flux<Shift> shift;
        if (employ != null && manager != null) {
            shift = shiftRepository.findByStartAtLessThanEqualAndEndAtGreaterThanEqualAndEmployNameAndManagerName(startAt, endAt, employ, manager);
        } else if (employ != null) {
            shift = shiftRepository.findByStartAtLessThanEqualAndEndAtGreaterThanEqualAndEmployName(startAt, endAt, employ);
        } else if (manager != null) {
            shift = shiftRepository.findByStartAtLessThanEqualAndEndAtGreaterThanEqualAndManagerName(startAt, endAt, manager);
        }

        return Flux.fromIterable(periods)
                .flatMap((timePeriod) -> {
                    Flux<ItemCollection> itemCollection;
                    if (stage == 0) {
                        itemCollection = itemRepository.findByTimestampBetween(timePeriod.getValue(), timePeriod.getKey());
                    } else {
                        itemCollection = itemRepository.findByTimestampBetweenAndStage(timePeriod.getValue(), timePeriod.getKey(), stage);
                    }
                    return itemCollection.collectList().map((List<ItemCollection> items) -> {
                        double count = items.stream().mapToInt(ItemCollection::getNoOfItems).average().orElse(0);
                        double defects = items.stream().mapToInt(ItemCollection::getNoOfDefected).average().orElse(0);
                        return new ReportElement(timePeriod.getValue().toString(), count, defects);
                    });
                }).collectList();
    }

    @CrossOrigin
    @GetMapping(value = "/employeeReport")
    Mono<List<EmployeeReport>> getEmployeeReport(@RequestParam(value = "endAt") String endAtReq) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/d/yyyy");
        LocalDateTime endAt = LocalDate.parse(endAtReq, formatter).atStartOfDay();
        LocalDateTime startAt = endAt.minusMonths(1);


        return employeeRepository.findAll().flatMap(employee -> {
            System.out.println(employee.getEmployName());
            return employAnalyticRepository.findByStartAtGreaterThanEqualAndEndAtLessThanEqualAndEmployName(startAt, endAt, employee.getEmployName())
                    .collectList()
                    .map(employAnalytics -> {
                        employAnalytics.forEach(employAnalytic -> {
                            System.out.println(employAnalytic.getAverageProcessRate());
                        });
                        double processAverage = employAnalytics.stream().filter(employAnalytic -> Double.isFinite(employAnalytic.getAverageProcessRate())).mapToDouble(EmployAnalytic::getAverageProcessRate).average().orElse(0);
                        double defectAverage = employAnalytics.stream().filter(employAnalytic -> Double.isFinite(employAnalytic.getAverageErrorRate())).mapToDouble(EmployAnalytic::getAverageErrorRate).average().orElse(0);
                        return new EmployeeReport(employee.getEmployName(), processAverage, defectAverage);
                    });
        }).collectList();
    }

    @CrossOrigin
    @GetMapping(value = "/notification/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<NotificationCollection> getNotificationsStream() {
        System.out.println("notification");

        return Flux.interval(Duration.ofMinutes(2)).flatMap((index) -> {
            System.out.println("notification interval");
            LocalDateTime oldTimestamp = lastNotificationUpdate;
            LocalDateTime newTimestamp = LocalDateTime.now();
            lastNotificationUpdate = newTimestamp;

            return calculateDeviation(oldTimestamp, newTimestamp);
        });
    }

    @CrossOrigin
    @GetMapping(value = "/notification")
    public Mono<NotificationCollection> getNotifications() {
        System.out.println("notification");
        LocalDateTime newTimestamp = lastNotificationUpdate;
        LocalDateTime oldTimestamp = newTimestamp.minusMinutes(10);

        return calculateDeviation(oldTimestamp, newTimestamp);
    }

    private Mono<NotificationCollection> calculateDeviation(LocalDateTime startAt, LocalDateTime endAt) {
        int threshold = 1;

        return Flux.range(1, 5).flatMap(stage -> analyticRepository.findByStartAtGreaterThanEqualAndEndAtLessThanEqualAndStage(startAt, endAt, stage)
                .collectList()
                .map((analyticList) -> new Pair<>(stage, analyticList.stream().mapToDouble(Analytic::getAverageProcessRate).average().orElse(0)))).collectList().map(averages -> {
            double overallAverage = averages.stream().mapToDouble(Pair::getValue).average().orElse(0);
            ArrayList<Notification> notificationList = new ArrayList<>();
            averages.forEach(average1 -> {
                System.out.println("averages" + average1.getValue() + " " + overallAverage);
                if (average1.getValue() - overallAverage > threshold) {
                    Pair<Integer, Double> min = averages.stream().min((a, b) -> (int) (a.getValue() - b.getValue())).orElse(new Pair<>(0, 0.0));
                    Notification notification = new Notification(average1.getKey(), min.getKey(), 1);
                    notificationList.add(notification);
                    System.out.println("Over treshold");

                } else if (average1.getValue() - overallAverage < -threshold) {
                    Pair<Integer, Double> max = averages.stream().max((a, b) -> (int) (a.getValue() - b.getValue())).orElse(new Pair<>(0, 0.0));
                    Notification notification = new Notification(max.getKey(), average1.getKey(), 1);
                    System.out.println("Over unedr");
                    notificationList.add(notification);
                }
            });
            return new NotificationCollection(notificationList);
        });
    }

    @GetMapping("/processEmployAnalytics")
    public Mono<List<List<EmployAnalytic>>> processEmployAnalytics() {
        System.out.println("processEmployAnalytics");
//        LocalDateTime oldTimestamp = LocalDateTime;
        LocalDateTime newTimestamp = LocalDateTime.now();
        LocalDateTime oldTimestamp = newTimestamp.minusHours(1);
        lastEmployAnalyticUpdate = newTimestamp;
        return shiftRepository.findByStartAtGreaterThanEqualAndEndAtLessThanEqual(oldTimestamp, newTimestamp).collectList().flatMap(shiftList -> Flux.range(1, 5).flatMap(stage -> {
            System.out.println("stage " + stage.toString());
            return analyticRepository.findByStartAtGreaterThanEqualAndEndAtLessThanEqualAndStage(oldTimestamp, newTimestamp, stage)
                    .collectList()
                    .flatMap(analytics -> {
                        System.out.println("analytics " + analytics.toString());
                        long count = shiftList.stream().filter(a -> a.getStage() == stage).count();
                        double processRateInShift = analytics.stream().mapToDouble(Analytic::getAverageProcessRate).average().orElse(0) / count;
                        double defectRateInShift = analytics.stream().mapToDouble(Analytic::getAverageErrorRate).average().orElse(0) / count;
                        return Flux.fromIterable(shiftList).flatMap(shift -> {
                            System.out.println("shift " + shift.getStage());
                            EmployAnalytic employAnalytic = new EmployAnalytic(shift.getEmployName(), processRateInShift, defectRateInShift, oldTimestamp, newTimestamp);
                            return employAnalyticRepository.save(employAnalytic);
                        }).collect(Collectors.toList());
                    });
        }).collectList());
    }
}
