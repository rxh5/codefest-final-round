package com.rxh5.codefest_round_1.controller;

import com.rxh5.codefest_round_1.model.Employee;
import com.rxh5.codefest_round_1.model.Shift;
import com.rxh5.codefest_round_1.model.ShiftRequest;
import com.rxh5.codefest_round_1.repository.EmployeeRepository;
import com.rxh5.codefest_round_1.repository.ShiftRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

@RestController
public class ShiftController {

    private final ShiftRepository shiftRepository;
    private final EmployeeRepository employeeRepository;

    private LocalDateTime lastShiftImport = LocalDateTime.now();

    @Autowired
    ShiftController(ShiftRepository shiftRepository, EmployeeRepository employeeRepository) {
        this.shiftRepository = shiftRepository;
        this.employeeRepository = employeeRepository;
    }

    @PostMapping("/shifts")
    public Mono<List<Shift>> createShifts(@Valid @RequestBody ShiftRequest request) {
        System.out.println("Create shift: " + request.toString());
        return Flux.fromIterable(request.getShiftList())
                .flatMap(shift -> {
                    shift.setStartAt(lastShiftImport);
                    lastShiftImport = LocalDateTime.now();
                    shift.setEndAt(lastShiftImport);
                    return shiftRepository.save(shift);
                }).collectList();
    }

    @PostMapping("/updateEmployees")
    public Mono<List<Employee>> updateEmployees(@Valid @RequestBody List<String> employees) {
        System.out.println("Update employees: " + employees.toString());
        return Flux.fromIterable(employees)
                .flatMap(employee -> employeeRepository.save(new Employee(employee))).collectList();
    }

}
