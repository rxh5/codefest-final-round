package com.rxh5.codefest_round_1;

import com.rxh5.codefest_round_1.model.Analytic;
import com.rxh5.codefest_round_1.model.Item;
import com.rxh5.codefest_round_1.model.ItemCollection;
import com.rxh5.codefest_round_1.model.ItemCollectionRequest;
import com.rxh5.codefest_round_1.repository.AnalyticRepository;
import com.rxh5.codefest_round_1.repository.ItemRepository;
import com.rxh5.codefest_round_1.service.ShiftIntegrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.WatchService;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;

@SpringBootApplication
public class CodefestFinalApplication {

    private final ItemRepository itemRepository;
    private final AnalyticRepository analyticRepository;

    private LocalDateTime analyticLastUpdate = LocalDateTime.now();

    @Autowired
    public CodefestFinalApplication(ItemRepository itemRepository, AnalyticRepository analyticRepository) {
        this.itemRepository = itemRepository;
        this.analyticRepository = analyticRepository;
    }

    @Bean
    WebClient webClient() {
        return WebClient.create("http://localhost:8080");
    }

    @Bean
    CommandLineRunner commandLineRunner(WebClient webClient) {
        return strings -> {
            syncWithShiftManagement(webClient);

            performAnalytics();

            performEmployeeAnalytics(webClient);

            emulateIOTDevice(webClient, 1);
            emulateIOTDevice(webClient, 2);
            emulateIOTDevice(webClient, 3);
            emulateIOTDevice(webClient, 4);
            emulateIOTDevice(webClient, 5);
        };
    }

    public static void main(String[] args) {
        SpringApplication.run(CodefestFinalApplication.class, args);
    }

    private void emulateIOTDevice(WebClient webClient, int stage) {
        Random random = new Random();

        Flux.interval(Duration.ofSeconds(1)).flatMap(
                (Long index) -> {
                    int randomNum = ThreadLocalRandom.current().nextInt(90, 121);
                    Item[] items = new Item[randomNum];

                    for (int i = 0; i < randomNum; i++) {
                        items[i] = new Item(random.nextBoolean());
                    }

                    ItemCollectionRequest request = new ItemCollectionRequest(randomNum, items, stage, LocalDateTime.now());

                    return webClient.post().uri("/items")
                            .contentType(MediaType.APPLICATION_JSON_UTF8)
                            .accept(MediaType.APPLICATION_JSON_UTF8)
                            .body(Mono.just(request), ItemCollectionRequest.class)
                            .retrieve()
                            .bodyToMono(ItemCollectionRequest.class);
                }
        ).subscribe();
    }

    private void syncWithShiftManagement(WebClient client) {
        String DIRECTORY_TO_WATCH = "/home/udesh/shifts/";

        Path toWatch = Paths.get(DIRECTORY_TO_WATCH);
        if (toWatch == null) {
            throw new UnsupportedOperationException("Directory not found");
        }

        WatchService watchService;
        try {
            watchService = toWatch.getFileSystem().newWatchService();
            ShiftIntegrationService shiftIntegrationService = new ShiftIntegrationService(watchService, client);

            Thread th = new Thread(shiftIntegrationService, "ShiftIntegrationService");
            th.start();

            // register a file
            toWatch.register(watchService, ENTRY_CREATE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void performAnalytics() {
        System.out.println("performAnalytics");
        Flux.interval(Duration.ofMinutes(1)).flatMap(index -> {
            System.out.println("interval");
            LocalDateTime oldTimestamp = analyticLastUpdate;
            LocalDateTime newTimestamp = LocalDateTime.now();
            analyticLastUpdate = newTimestamp;
            System.out.println(oldTimestamp.toString() + " " + newTimestamp.toString());
            return Flux.range(1, 5).flatMap(stage -> itemRepository.findByTimestampBetweenAndStage(oldTimestamp, newTimestamp, stage).collectList().flatMap(itemCollections -> {
                double currentProcessAverage = itemCollections.stream().mapToInt(ItemCollection::getNoOfItems).average().orElse(0);
                double currentDefectAverage = itemCollections.stream().mapToInt(ItemCollection::getNoOfDefected).average().orElse(0);
                Analytic currentAnalytic = new Analytic(stage, currentProcessAverage, currentDefectAverage, oldTimestamp, newTimestamp);
                return analyticRepository.save(currentAnalytic);
            }));
        }).subscribe(
                val -> {
                    System.out.println("Created analytic: " + val.getId());
                }, err -> {
                    System.out.println(err.getMessage());
                }
        );
    }

    private void performEmployeeAnalytics(WebClient webClient) {
        System.out.println("performEmployeeAnalytics");
        Flux.interval(Duration.ofHours(1)).subscribe(index -> {
            System.out.println("performEmployeeAnalytics rate");
            webClient.get().uri("/processEmployAnalytics").retrieve().bodyToMono(List.class).subscribe();
        });
    }

}
