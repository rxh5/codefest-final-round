package com.rxh5.codefest_round_1.repository;

import com.rxh5.codefest_round_1.model.Analytic;
import com.rxh5.codefest_round_1.model.Shift;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

import java.time.LocalDateTime;

@Repository
public interface AnalyticRepository extends ReactiveMongoRepository<Analytic, String> {

    Flux<Analytic> findByStartAtGreaterThanEqualAndEndAtLessThanEqualAndStage(LocalDateTime startAt, LocalDateTime endAt, int stage);

}