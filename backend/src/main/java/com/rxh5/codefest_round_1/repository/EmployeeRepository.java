package com.rxh5.codefest_round_1.repository;

import com.rxh5.codefest_round_1.model.EmployAnalytic;
import com.rxh5.codefest_round_1.model.Employee;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

import java.time.LocalDateTime;

@Repository
public interface EmployeeRepository extends ReactiveMongoRepository<Employee, String> {

}