package com.rxh5.codefest_round_1.repository;

import com.rxh5.codefest_round_1.model.Analytic;
import com.rxh5.codefest_round_1.model.EmployAnalytic;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

import java.time.LocalDateTime;

@Repository
public interface EmployAnalyticRepository extends ReactiveMongoRepository<EmployAnalytic, String> {

    Flux<EmployAnalytic> findByStartAtGreaterThanEqualAndEndAtLessThanEqual(LocalDateTime startAt, LocalDateTime endAt);

    Flux<EmployAnalytic> findByStartAtLessThanEqualAndEndAtGreaterThanEqual(LocalDateTime startAt, LocalDateTime endAt);

    Flux<EmployAnalytic> findByStartAtLessThanEqualAndEndAtGreaterThanEqualAndEmployName(LocalDateTime startAt, LocalDateTime endAt, String employName);

    Flux<EmployAnalytic> findByStartAtGreaterThanEqualAndEndAtLessThanEqualAndEmployName(LocalDateTime startAt, LocalDateTime endAt, String employName);

}