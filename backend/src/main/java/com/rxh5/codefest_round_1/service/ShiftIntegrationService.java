package com.rxh5.codefest_round_1.service;

import com.rxh5.codefest_round_1.model.ItemCollectionRequest;
import com.rxh5.codefest_round_1.model.Shift;
import com.rxh5.codefest_round_1.model.ShiftRequest;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;

public class ShiftIntegrationService implements Runnable {

    private WatchService watcher;
    private WebClient webClient;

    public ShiftIntegrationService(WatchService watcher, WebClient webClient) {
        this.watcher = watcher;
        this.webClient = webClient;
    }

    @Override
    public void run() {
        try {
            WatchKey key = watcher.take();
            while (key != null) {
                for (WatchEvent event : key.pollEvents()) {
                    if (event.kind() == ENTRY_CREATE || event.kind() == ENTRY_MODIFY) {
                        System.out.printf("Received %s event for file: %s\n",
                                event.kind(), event.context());
                        String line;
                        String cvsSplitBy = ",";
                        if (event.context().toString().equals("data.csv")) {
                            String csvFile = "/home/udesh/shifts/data.csv";

                            try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
                                ArrayList<Shift> list = new ArrayList<>();
                                while ((line = br.readLine()) != null) {
                                    String[] data = line.split(cvsSplitBy);
                                    Shift shift = new Shift();
                                    shift.setEmployName(data[0]);
                                    shift.setManagerName(data[1]);
                                    shift.setStage(Integer.parseInt(data[2]));
                                    list.add(shift);
                                }
                                ShiftRequest request = new ShiftRequest(list);
                                webClient.post().uri("/shifts")
                                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                                        .accept(MediaType.APPLICATION_JSON_UTF8)
                                        .body(Mono.just(request), ShiftRequest.class)
                                        .retrieve()
                                        .bodyToMono(ItemCollectionRequest.class)
                                        .subscribe();
                                webClient.get().uri("/processEmployAnalytics").retrieve().bodyToMono(List.class).subscribe();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } else if (event.context().toString().equals("employee.csv")) {
                            String csvFile = "/home/udesh/shifts/employee.csv";

                            try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
                                ArrayList<String> request = new ArrayList<>();
                                while ((line = br.readLine()) != null) {
                                    String[] data = line.split(cvsSplitBy);
                                    request.addAll(Arrays.asList(data));
                                }
                                webClient.post().uri("/updateEmployees")
                                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                                        .accept(MediaType.APPLICATION_JSON_UTF8)
                                        .body(Mono.just(request), List.class)
                                        .retrieve()
                                        .bodyToMono(List.class)
                                        .subscribe();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
                key.reset();
                key = watcher.take();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Stopping thread");
    }
}