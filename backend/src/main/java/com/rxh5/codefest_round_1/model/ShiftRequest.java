package com.rxh5.codefest_round_1.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Model to hold shift request
 *
 * @author Udesh Kumarasinghe */
public class ShiftRequest {

    private List<Shift> shiftList;

    public ShiftRequest() {}

    public ShiftRequest(List<Shift> shiftList) {
        this.shiftList = shiftList;
    }

    // Setters
    public void setShiftList(List<Shift> shiftList) {
        this.shiftList = shiftList;
    }

    // Getters
    public List<Shift> getShiftList() {
        return shiftList;
    }
}
