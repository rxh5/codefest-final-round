package com.rxh5.codefest_round_1.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Model to represent item data, contain stage, whether item is defective or not.
 *
 * @author Udesh Kumarasinghe */
public class Item {

    @NotNull
    private boolean defective = false;

    public Item() {

    }

    public Item(boolean defective) {
        this.defective = defective;
    }

    // Getters
    public boolean isDefective() {
        return defective;
    }

    // Setters
    public void setDefective(boolean defective) {
        this.defective = defective;
    }

}
