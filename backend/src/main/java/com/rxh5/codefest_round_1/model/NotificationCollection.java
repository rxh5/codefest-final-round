package com.rxh5.codefest_round_1.model;

import java.util.List;

public class NotificationCollection {

    private List<Notification> notifications;

    public NotificationCollection() {

    }

    public NotificationCollection(List<Notification> notifications) {
        this.notifications = notifications;
    }

    // Setters
    public void setNotifications(List<Notification> notifications) {
        this.notifications = notifications;
    }

    // Getters
    public List<Notification> getNotifications() {
        return notifications;
    }

}
