package com.rxh5.codefest_round_1.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Document(collection = "employanalytic")
public class EmployAnalytic {

    @Id
    private String id;

    @NotNull
    private String employName;

    @NotNull
    private double averageProcessRate;

    @NotNull
    private double averageErrorRate;

    @NotNull
    private LocalDateTime startAt;

    @NotNull
    private LocalDateTime endAt;

    public EmployAnalytic() {

    }

    public EmployAnalytic(String employName, double averageProcessRate, double averageErrorRate, LocalDateTime startAt, LocalDateTime endAt) {
        this.employName = employName;
        this.averageProcessRate = averageProcessRate;
        this.averageErrorRate = averageErrorRate;
        this.startAt = startAt;
        this.endAt = endAt;
    }

    // Getters
    public String getId() {
        return id;
    }

    public String getEmployName() {
        return employName;
    }

    public double getAverageProcessRate() {
        return averageProcessRate;
    }

    public double getAverageErrorRate() {
        return averageErrorRate;
    }

    public LocalDateTime getStartAt() {
        return startAt;
    }

    public LocalDateTime getEndAt() {
        return endAt;
    }

    // Setters
    public void setId(String id) {
        this.id = id;
    }

    public void setEmployName(String employName) {
        this.employName = employName;
    }

    public void setAverageProcessRate(double averageProcessRate) {
        this.averageProcessRate = averageProcessRate;
    }

    public void setAverageErrorRate(double averageErrorRate) {
        this.averageErrorRate = averageErrorRate;
    }

    public void setStartAt(LocalDateTime startAt) {
        this.startAt = startAt;
    }

    public void setEndAt(LocalDateTime endAt) {
        this.endAt = endAt;
    }
}
