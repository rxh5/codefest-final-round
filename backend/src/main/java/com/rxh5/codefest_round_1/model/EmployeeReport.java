package com.rxh5.codefest_round_1.model;

public class EmployeeReport {

    private String name;
    private double processAverage;
    private double defectAverage;

    public EmployeeReport() {}

    public EmployeeReport(String name, double processAverage, double defectAverage) {
        this.name = name;
        this.processAverage = processAverage;
        this.defectAverage = defectAverage;
    }

    // Getters
    public String getName() {
        return name;
    }

    public double getProcessAverage() {
        return processAverage;
    }

    public double getDefectAverage() {
        return defectAverage;
    }

    // Setters
    public void setName(String name) {
        this.name = name;
    }

    public void setProcessAverage(double processAverage) {
        this.processAverage = processAverage;
    }

    public void setDefectAverage(double defectAverage) {
        this.defectAverage = defectAverage;
    }
}
