package com.rxh5.codefest_round_1.model;

import java.time.LocalDateTime;

public class ItemCollectionRequest extends BaseItemCollection {

    private Item[] items;

    public ItemCollectionRequest() {
        super();
    }

    public ItemCollectionRequest(int noOfItems, Item[] items, int stage, LocalDateTime timestamp) {
        super(noOfItems, stage, timestamp);
        this.items = items;
    }

    // Getters
    public Item[] getItems() {
        return items;
    }

    // Setters
    public void setItems(Item[] items) {
        this.items = items;
    }

}
