package com.rxh5.codefest_round_1.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * Model to represent shift data imported from Shift Management system
 *
 * @author Udesh Kumarasinghe */
@Document(collection = "shift")
public class Shift {

    @Id
    private String id;

    @NotNull
    private String employName;

    @NotNull
    private String managerName;

    private LocalDateTime startAt;

    private LocalDateTime endAt;

    @NotNull
    private int stage;

    public Shift() {

    }

    public Shift(String managerName, String employName, LocalDateTime startAt, LocalDateTime endAt, int stage) {
        this.managerName = managerName;
        this.employName = employName;
        this.startAt = startAt;
        this.endAt = endAt;
        this.stage = stage;
    }

    // Setters
    public void setId(String id) {
        this.id = id;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public void setEmployName(String employName) {
        this.employName = employName;
    }

    public void setStartAt(LocalDateTime startAt) {
        this.startAt = startAt;
    }

    public void setEndAt(LocalDateTime endAt) {
        this.endAt = endAt;
    }

    public void setStage(int stage) {
        this.stage = stage;
    }

    // Getters
    public String getId() {
        return id;
    }

    public String getEmployName() {
        return employName;
    }

    public String getManagerName() {
        return managerName;
    }

    public LocalDateTime getStartAt() {
        return startAt;
    }

    public LocalDateTime getEndAt() {
        return endAt;
    }

    public int getStage() {
        return stage;
    }
}
