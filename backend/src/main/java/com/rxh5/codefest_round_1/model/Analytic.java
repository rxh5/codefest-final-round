package com.rxh5.codefest_round_1.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Document(collection = "analytic")
public class Analytic {

    @Id
    private String id;

    @NotNull
    private int stage;

    @NotNull
    private double averageProcessRate;

    @NotNull
    private double averageErrorRate;

    @NotNull
    private LocalDateTime startAt;

    @NotNull
    private LocalDateTime endAt;

    public Analytic() {

    }

    public Analytic(int stage, double averageProcessRate, double averageErrorRate, LocalDateTime startAt, LocalDateTime endAt) {
        this.stage = stage;
        this.averageProcessRate = averageProcessRate;
        this.averageErrorRate = averageErrorRate;
        this.startAt = startAt;
        this.endAt = endAt;
    }

    // Getters
    public String getId() {
        return id;
    }

    public int getStage() {
        return stage;
    }

    public double getAverageProcessRate() {
        return averageProcessRate;
    }

    public double getAverageErrorRate() {
        return averageErrorRate;
    }

    public LocalDateTime getStartAt() {
        return startAt;
    }

    public LocalDateTime getEndAt() {
        return endAt;
    }

    // Setters
    public void setId(String id) {
        this.id = id;
    }

    public void setStage(int stage) {
        this.stage = stage;
    }

    public void setAverageProcessRate(double averageProcessRate) {
        this.averageProcessRate = averageProcessRate;
    }

    public void setAverageErrorRate(double averageErrorRate) {
        this.averageErrorRate = averageErrorRate;
    }

    public void setStartAt(LocalDateTime startAt) {
        this.startAt = startAt;
    }

    public void setEndAt(LocalDateTime endAt) {
        this.endAt = endAt;
    }
}
