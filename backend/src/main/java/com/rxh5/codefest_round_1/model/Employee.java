package com.rxh5.codefest_round_1.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "employee")
public class Employee {

    @Id
    private String employName;

    public Employee() {

    }

    public Employee(String employName) {
        this.employName = employName;
    }

    public String getEmployName() {
        return employName;
    }


    public void setEmployName(String employName) {
        this.employName = employName;
    }

}
