package com.rxh5.codefest_round_1.model;

public class Notification {

    private int fromStage;
    private int toStage;
    private int amount;

    public Notification() {

    }

    public Notification(int fromStage, int toStage, int amount) {
        this.toStage = toStage;
        this.fromStage = fromStage;
        this.amount = amount;
    }

    // Setters
    public void setFromStage(int fromStage) {
        this.fromStage = fromStage;
    }

    public void setToStage(int toStage) {
        this.toStage = toStage;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    // Getters
    public int getFromStage() {
        return fromStage;
    }

    public int getToStage() {
        return toStage;
    }

    public int getAmount() {
        return amount;
    }
}
