package com.rxh5.codefest_round_1.model;

public class ReportElement {

    private String label;

    private double noOfItems;

    private double noOfDefects;

    public ReportElement() {}

    public ReportElement(String label, double noOfItems, double noOfDefects) {
        this.label = label;
        this.noOfItems = noOfItems;
        this.noOfDefects = noOfDefects;
    }

    // Getters
    public String getLabel() {
        return label;
    }

    public double getNoOfItems() {
        return noOfItems;
    }

    public double getNoOfDefects() {
        return noOfDefects;
    }

    // Setters
    public void setLabel(String label) {
        this.label = label;
    }

    public void setNoOfItems(double noOfItems) {
        this.noOfItems = noOfItems;
    }

    public void setNoOfDefects(double noOfDefects) {
        this.noOfDefects = noOfDefects;
    }
}
