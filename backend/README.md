# Back-End

## Preparing the environment

A value for the threshold should enter to the system. If not, a default value will be assigned as the threshold value.

## Shifting Employees

Whenever the system recommand a re-arrangement of employees, user/manager can shift employees from one stage to another stage.

## Updating CSV file

If user/manager decides to shift an employee, the CSV file has to be updated according to the new arrangement.