import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Rate } from '../model/rate';

@Injectable()
export class ProcessingRateService {

  url = 'http://localhost:8080';

  constructor() { }

  getCurrentRateOfStageStream(stage: number): Observable<Rate> {
    return Observable.create((observer) => {
      const eventSource = new EventSource(this.url + '/items/' + stage);
      eventSource.onmessage = (event) => {
        // console.log('Received event: ', event);
        const json = JSON.parse(event.data);
        const time = new Date(json['timestamp']);
        observer.next(new Rate(json['noOfItems'], json['noOfDefected'] * 100 / json['noOfItems'],
          time.getHours().toString() + ':' + time.getMinutes().toString() + ':' + time.getSeconds().toString()));
      };
      eventSource.onerror = (error) => {
        // readyState === 0 (closed) means the remote source closed the connection,
        // so we can safely treat it as a normal situation. Another way of detecting the end of the stream
        // is to insert a special element in the stream of events, which the client can identify as the last one.
        if (eventSource.readyState === 0) {
          console.log('The stream has been closed by the server.');
          eventSource.close();
          observer.complete();
        } else {
          observer.error('EventSource error: ' + error);
        }
      };
    });
  }

  getNotificationListener(): Observable<any> {
    return Observable.create((observer) => {
      const eventSource = new EventSource(this.url + '/notification/stream');
      eventSource.onmessage = (event) => {
        // console.log('Received event: ', event);
        const json = JSON.parse(event.data);
        // const time = new Date(json['timestamp']);
        observer.next(json);
      };
      eventSource.onerror = (error) => {
        // readyState === 0 (closed) means the remote source closed the connection,
        // so we can safely treat it as a normal situation. Another way of detecting the end of the stream
        // is to insert a special element in the stream of events, which the client can identify as the last one.
        if (eventSource.readyState === 0) {
          console.log('The stream has been closed by the server.');
          eventSource.close();
          observer.complete();
        } else {
          observer.error('EventSource error: ' + error);
        }
      };
    });
  }

}
