import { Component, OnInit } from '@angular/core';
// import * as Chartist from 'chartist';
import { ProcessingRateService } from '../services/processing-rate.service';
import { Chart } from 'chart.js';
import { RateQueue } from '../util/rate-queue';
import { Observable, Subscription } from 'rxjs';
import { Rate } from '../model/rate';
import swal from 'sweetalert';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: [ProcessingRateService]
})
export class DashboardComponent implements OnInit {

  subscription: Subscription;

  constructor(private processingRateService: ProcessingRateService) {

  }

  ngOnInit() {
    this.subscription = this.processingRateService.getNotificationListener().subscribe(
      (json) => {
        console.log(json);
        if (json.notifications.length > 0) {
          const suggession = 'Low process rate in stage ' + json.notifications[0].toStage +
          '. \n\nSugestion: Move employee from stage ' + json.notifications[0].fromStage + ' to stage ' + json.notifications[0].toStage;
          swal(suggession);
        }
      }
    );
  }

}
