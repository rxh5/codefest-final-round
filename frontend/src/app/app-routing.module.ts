import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from './dashboard/dashboard.component';
import {ReportsComponent} from './reports/reports.component';
import {EmployeeReportComponent} from './employee-report/employee-report.component';
import {DiagnosisComponent} from './diagnosis/diagnosis.component';


const routes: Routes = [
  {path: '', redirectTo: '/dashboard', pathMatch: 'full'},
  {path: 'dashboard', component: DashboardComponent},
  // {path: 'home', component: HomeComponent},
  {path: 'reports', component: ReportsComponent},
  {path: 'employeeReports', component: EmployeeReportComponent},
  {path: 'diagnosis', component: DiagnosisComponent},
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {}
