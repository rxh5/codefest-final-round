import { Component, OnInit } from '@angular/core';
import { ProcessingRateService } from '../services/processing-rate.service';

@Component({
  selector: 'app-diagnosis',
  templateUrl: './diagnosis.component.html',
  styleUrls: ['./diagnosis.component.css']
})
export class DiagnosisComponent implements OnInit {

  isDef1: Boolean;
  isDef2: Boolean;
  isDef3: Boolean;
  isDef4: Boolean;
  isDef5: Boolean;

  constructor(private processingRateService: ProcessingRateService) {
  }

  ngOnInit() {
      // this.isDef3 = true;
      this.processingRateService.getCurrentRateOfStageStream(1).subscribe(
        (stageRate) => {
          if (stageRate.processRate === 0) {
            this.isDef1 = true;
          } else {
            this.isDef1 = false;
          }
        }
      );
      this.processingRateService.getCurrentRateOfStageStream(2).subscribe(
        (stageRate) => {
          if (stageRate.processRate === 0) {
            this.isDef2 = true;
          } else {
            this.isDef2 = false;
          }
        }
      );
      this.processingRateService.getCurrentRateOfStageStream(3).subscribe(
        (stageRate) => {
          if (stageRate.processRate === 0) {
            this.isDef3 = true;
          } else {
            this.isDef3 = false;
          }
        }
      );
      this.processingRateService.getCurrentRateOfStageStream(4).subscribe(
        (stageRate) => {
          if (stageRate.processRate === 0) {
            this.isDef4 = true;
          } else {
            this.isDef4 = false;
          }
        }
      );
      this.processingRateService.getCurrentRateOfStageStream(5).subscribe(
        (stageRate) => {
          if (stageRate.processRate === 0) {
            this.isDef5 = true;
          } else {
            this.isDef5 = false;
          }
        }
      );
    // }

  }

}
