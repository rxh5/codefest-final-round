import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatIconModule} from '@angular/material/icon';

import { AppComponent } from './app.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {ProcessingRateService} from './services/processing-rate.service';
import {MaterialModule} from './material.module';
import {StageComponent} from './stage/stage.component';
import {ChartComponent} from './chart/chart.component';
import {FormsModule} from '@angular/forms';
import {ReportsComponent} from './reports/reports.component';
import {AppRoutingModule} from './app-routing.module';
import {EmployeeReportComponent} from './employee-report/employee-report.component';
import {DiagnosisComponent} from './diagnosis/diagnosis.component';
import {SweetAlert2Module} from '@toverux/ngx-sweetalert2';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    StageComponent,
    ReportsComponent,
    ChartComponent,
    EmployeeReportComponent,
    DiagnosisComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    SweetAlert2Module.forRoot(),
  ],
  providers: [ProcessingRateService],
  bootstrap: [AppComponent]
})
export class AppModule { }
