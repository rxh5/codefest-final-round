import {MatButtonModule, MatCardModule, MatToolbarModule, MatGridListModule, MatSidenavModule, MatIconModule, MatListModule, MatFormFieldModule, MatDatepickerModule, MatNativeDateModule, MatOptionModule, MatSelectModule, MatFormFieldControl, MatInputModule,} from '@angular/material';
import {MatTableModule} from '@angular/material/table';
import {NgModule} from '@angular/core';

@NgModule({
  imports: [MatButtonModule, MatToolbarModule, MatCardModule, MatGridListModule, MatSidenavModule, MatIconModule, MatListModule, MatFormFieldModule, MatDatepickerModule, MatNativeDateModule, MatOptionModule, MatSelectModule, MatInputModule, MatTableModule],
  exports: [MatButtonModule, MatToolbarModule, MatCardModule, MatGridListModule, MatSidenavModule, MatIconModule, MatListModule, MatFormFieldModule, MatDatepickerModule, MatNativeDateModule, MatOptionModule, MatSelectModule, MatInputModule, MatTableModule],
})
export class MaterialModule {}
