import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

export interface EmployeeDetail {
  name: number;
  processAverage: number;
  defectAverage: number;
}

const ELEMENT_DATA: EmployeeDetail[] = [
  {name: 1, processAverage: 200, defectAverage: 3},
];


@Component({
  selector: 'app-employee-report',
  templateUrl: './employee-report.component.html',
  styleUrls: ['./employee-report.component.css']
})
export class EmployeeReportComponent implements OnInit {

  private baseUrl = 'http://localhost:8080/employeeReport';

  displayedColumns: string[] = ['id', 'productionAverage', 'defectsAverage'];
  dataSource = ELEMENT_DATA;

  constructor(private httpClient: HttpClient) { }

  ngOnInit() {
  }

  getValues(date) {
    console.log(date);
    this.httpClient.get(this.baseUrl + '?endAt=' + date).subscribe((res: any[]) => {
      console.log(res);
      this.dataSource = res;
    });
  }


}
