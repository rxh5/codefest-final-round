import { Component, OnInit, Input } from '@angular/core';
// import {ProcessingRateService} from '../services/processing-rate.service';
import {Chart} from 'chart.js';
// import {RateQueue} from '../util/rate-queue';
import {Observable} from 'rxjs';
// import {Rate} from '../model/rate';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
  // providers: [ProcessingRateService]
})
export class ChartComponent implements OnInit {

  chart = [];

  constructor() { }

  ngOnInit() {
  }

}
