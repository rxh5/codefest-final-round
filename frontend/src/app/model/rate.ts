export class Rate {

  processRate: number;
  defectivePersentage: number;
  timestamp: string;

  constructor(rate: number, defects: number, timestamp: string) {
    this.processRate = rate;
    this.defectivePersentage = defects;
    this.timestamp = timestamp;
  }
}
