import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Chart } from 'chart.js';

export interface Stage {
  value: string;
  viewValue: string;
}

export interface Period {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})

export class ReportsComponent implements OnInit {

  // private pro: string;

  private baseUrl = 'http://localhost:8080/report';

  chart1 = [];
  chart2 = [];
  label = [];
  item = [];
  defects = [];

  stages: Stage[] = [
    { value: '1', viewValue: '1' },
    { value: '2', viewValue: '2' },
    { value: '3', viewValue: '3' },
    { value: '4', viewValue: '4' },
    { value: '5', viewValue: '5' }
  ];

  periods: Period[] = [
    { value: 'year', viewValue: 'Year' },
    { value: 'month', viewValue: 'Month' },
    { value: 'week', viewValue: 'Week' },
    { value: 'day', viewValue: 'Day' },
    { value: 'hour', viewValue: 'Hour' }
  ];

  constructor(private httpClient: HttpClient) { }

  ngOnInit() {
  }

  getValues(startAt, endAt, stage, period, employ, manager) {
    this.httpClient.get(this.baseUrl + '?startAt=' + startAt + '&endAt=' + endAt + '&period=' + period + '&stage=' +
      stage + '&employ=' + employ + '&manager=' + manager).subscribe((res: any[]) => {
        this.label = [];
        this.item = [];
        this.defects = [];
        res.forEach(element => {
          this.label.push(element.label);
          this.item.push(element.noOfItems);
          this.defects.push(element.noOfDefects);
          console.log(element);
        });
        this.chart1 = new Chart('report1', {
          type: 'line',
          data: {
            labels: this.label,
            datasets: [
              {
                data: this.item,
                borderColor: '#0D47A1',
                backgroundColor: '#0D47A155',
              },
            ],
          },
          options: {
            tooltips: {
              enabled: false,
            },
            hover: {
              mode: null
            },
            animation: {
              duration: 0,
            },
            legend: {
              display: false
            },
            scales: {
              xAxes: [{
                display: true,
              }],
              yAxes: [{
                display: true,
                ticks: {
                  max: 140,
                  beginAtZero: true,
                }
              }],
            }
          }
        });
        this.chart2 = new Chart('report2', {
          type: 'line',
          data: {
            labels: this.label,
            datasets: [
              {
                data: this.defects,
                borderColor: '#FF0000',
                backgroundColor: '#FF000055',
              },
            ],
          },
          options: {
            tooltips: {
              enabled: false,
            },
            hover: {
              mode: null
            },
            animation: {
              duration: 0,
            },
            legend: {
              display: false
            },
            scales: {
              xAxes: [{
                display: true,
              }],
              yAxes: [{
                display: true,
                ticks: {
                  max: 140,
                  beginAtZero: true,
                }
              }],
            }
          }
        });
      });
  }

}
