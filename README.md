# RxH5 | Codefest Final Round

_Realtime production line monitoring system using data from IOT devices._

## Team

* Udesh Kumarasinghe
* Sashika Nawarathne
* Ilthizam Imtiyaz
* Prabhani Nanayakkara

## About

* Realtime web dashboard
  * Graph for each stage.
  * Each graph contains the details of the production rate and the error rate of current time.
* Report
  * Report contains past performance and error rates of each stage with the employees and managers. It can be customized according to the production managers needs.
* Realtime recommendations on how to re-arrange the employees to optimise the production line
  * System calculates the average of the production rate in each stage in every 10 minutes.
  * Each value compares with a threshold.
  * If the average value is above or below the threshold, system notifies the user about the deviasion and suggest a solution.
* Report of employees performance during a month
  * Gives a complete table of employees performance and error rates during the month
  * Gives a list of employees whose performance is below 70% of average employee performance and error rate is above 20% of average employee error rate. 
* Detect IOT device failures
  * Detecting defects in stages and notifies the administrator immdiately.
* A single web UI to diagnose the issues
  * Displays the production failures amd notifies the administrator.

## Technologies Used

* Java 8 for the back-end
* Spring Boot 5 with WebFlux to have a reactive back-end
* Angular 6 for the front-end
* MongoDB as the datasource

## How to run

* Clone the repository
* Change current directory to the cloned repository
* Start the MongoDB service `$ sudo service mongod start`
* Start the back-end
  * Start a new terminal
  * Change current directory to back-end `cd backend`
  * Start Spring Boot back-end `$ ./gradlew bootRun`
* Start the front-end
  * Start a new terminal
  * Change current directory to front-end `cd frontend`
  * Run Angular front-end `$ ng serve --open`
* Dashboard will be hosted on `localhost:4200`

## Third Party Dependencies

* Springboot mongoDB reactive starter
* Springboot WebFlux starter
* Springboot test starter
* Reactor test
* Bootstrap
* Chart.js
* Rx.js
* Core-js
* Zone.js